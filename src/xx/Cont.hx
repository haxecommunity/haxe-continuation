package xx;

// a continuation function taking a LazyVal as first argument
typedef ContP<X> = (LazyVal<X>) -> Void;

// a function taking a continuation function as parameter
typedef Cont<X> = ContP<X> -> Void;

