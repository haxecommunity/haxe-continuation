package xx;

// a value wrapped in a callable function so that an exception can be thrown
// instead
typedef LazyVal<X> = Void -> X;

