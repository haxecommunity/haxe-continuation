package xx;
import haxe.macro.Expr;
import haxe.macro.Context;
import xx.Cont;

class ContUtil {

  // catch Exceptions and pass them to continuation functions
  macro static public function catchCont(cont:Expr, block:Expr):Expr {
    return macro {
      try{
        var r = $block;
        $cont(function(){ return r; });
      }catch(e:Dynamic){
        $cont(function(){ throw e; return null; });
      }
    }
  }


  macro static public function cont(block:Expr):Expr {
    return macro {
      function(cont){
        try{
          var r = $block;
          cont(function(){ return r; });
        }catch(e:Dynamic){
          cont(function(){ throw e; return null; });
        }
      }
    };
  }

  static public function onException<T>(c_exception:Cont<T>, onException: LazyVal<T>):Cont<T> {
    return function(cont){
      c_exception(function(v){
        try{
          var value = v();
          cont(function(){ return value; });
        }catch(e:Dynamic){
          cont(onException);
        }
      });
    };
  }
}
